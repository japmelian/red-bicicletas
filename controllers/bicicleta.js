var Bicicleta = require('../models/bicicleta');

module.exports = {
	list: function(req, res, next) {
		Bicicleta.find({}, (err, bicicletas) => {
			res.render('bicicletas/', { bicis: bicicletas });
		});
	},

	create_get: function(req, res, next) {
        res.render('bicicletas/create', { errors: {}, bicicleta: new Bicicleta() });
	},
	
    create: function(req, res, next) {
        Bicicleta.create({ code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]}, function(err, nuevaBici) {
            if (err) {
				console.log('error creando bicicleta');
                //res.render('bicicletas/create', { errors: err.errors, bicicleta: new Bicicleta({ nombre: req.body.nombre, email: req.body.email }) });
            } else {
                res.redirect('/bicicletas');
            }
        });
	},
	
	update: function(req, res, next) {
        var update_values = { code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng] };
        
        Bicicleta.findByIdAndUpdate(req.params.id, update_values, function(err, bicicleta) {
            if (err) {
                console.log(err);
                res.render('bicicletas/update', { errors: err.errors, bici: new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng] }) });
            } else {
                res.redirect('/bicicletas');
                return;
            }
        });
	},
	
	update_get: function(req, res, next) {
		console.log(req);
        Bicicleta.findById(req.params.id, function(err, bicicleta) {
			console.log(bicicleta);
            res.render('bicicletas/update', { errors: {}, bici: bicicleta });
        });
	},
	
	delete: function(req, res, next) {
        Bicicleta.findByIdAndDelete(req.body.id, function(err) {
            if (err)
                next(err);
            else
                res.redirect('/bicicletas');
        });
    }
}

/*
exports.bicicleta_list = function (req, res) {
	// Hay que crear una carpeta/fichero en views que se llama /bicicletas/index.pug
	res.render('bicicletas/index', { bicis: Bicicleta.allBicis });
}

exports.bicicleta_create_get = function (req, res) {
	res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res) {
	var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);

	bici.ubicacion_ = [req.body.lat, req.body.lng];

	Bicicleta.add(bici);

	res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function (req, res) {
	var bici = Bicicleta.findById(req.params.id);
	res.render('bicicletas/update', { bici });
}

exports.bicicleta_update_post = function (req, res) {
	var bici = Bicicleta.findById(req.params.id);

	bici.id_ = req.body.id;
	bici.color_ = req.body.color;
	bici.modelo_ = req.body.modelo;
	bici.ubicacion_ = [req.body.lat, req.body.lng];

	res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function (req, res) {
	Bicicleta.removeById(req.body.id);

	res.redirect('/bicicletas');
}
*/