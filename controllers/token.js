var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmationGet: function(req, res, next) {
        Token.findOne({ token: req.params.token }, function(err, token) {
            if (!token) return res.status(400).send({ type: 'not-verified', msg: 'no se encontro usuario con ese token' });

            Usuario.findById(token._userId, function(err, usuario) {
                if (!usuario) return res.status(400).send({ msg: 'usuario no encontrado' });

                if (usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save(function(err) {
                    if (err) { return res.status(500).send({ msn: err.message }); }
                    res.redirect('/')
                });
            });
        });
    }
}