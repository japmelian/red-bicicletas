var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');


var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function(done) {
        var mongodb = 'mongodb://localhost/testdb';
        mongoose.connect(mongodb, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;

        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS/', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe(' POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            
            var headers = {'content-type' : 'application/json'};
            
            var aBici = '{"code":1, "color":"azul", "modelo":"Urbano", "lat":"10.974273", "lng":"-74.815885"}';

            request.post({
                headers : headers,
                url : 'http://localhost:3000/api/bicicletas/create',
                body : aBici
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(1, ( err, bicicleta) => {
                    if ( err) console.log(err);
                    //console.log(bicicleta);
                    expect(bicicleta.code).toBe(1);
                    done();
                });
                
            });
        
        });
    });
});

/*
beforeEach(() => { Bicicleta.allBicis = [] });

describe('API Bicicletas', () => {
    describe('GET bicicleta', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a  = new Bicicleta(1, 'negro', 'urbana', [-1, -1]);

            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            })
        })
    })

    describe('POST bicicleta /create', () => {
        it('Status 200', (done) => {
            var h = { 'content-type': 'application/json' };
            var a = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": 10, "lng": 10 }';

            request.post({
                headers: h,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: a
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color_).toBe('rojo');
                done();
            })
        })
    })

    describe('DELETE bicicleta /delete', () => {
        it('Status 204', (done) => {
            var h = { 'content-type': 'application/json' };
            var a  = new Bicicleta(1, 'negro', 'urbana', [-1, -1]);
            var bici_delete = '{ "id": 1 }';

            Bicicleta.add(a);

            request.delete({
                headers: h,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: bici_delete
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
            })
        })
    })

    describe('POST bicicleta /update', () => {
        it('Status 200', (done) => {
            var h = { 'content-type': 'application/json' };
            var a  = new Bicicleta(1, 'negro', 'urbana', [-1, -1]);
            var b = '{ "id": 1, "color": "rojo", "modelo": "montaña", "lat": -1, "lng": -2 }';

            Bicicleta.add(a);

            request.post({
                headers: h,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: b
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color_).toBe('rojo');
                done();
            })
        })
    })
})
*/