var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
const { abort } = require('process');

describe('Testing Bicicletas', function() {
    beforeEach(function(done) {
        var mongodb = 'mongodb://localhost/testdb';
        mongoose.connect(mongodb, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log("we are connected to the test database");
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [1, -1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(1);
            expect(bici.ubicacion[1]).toEqual(-1);
        });
    });

    describe('Bicicletas.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });

            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });

                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({ code: 2, color: 'rojo', modelo: 'montaña' });

                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function(error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });
});

/*

beforeEach(() => { Bicicleta.allBicis = [] });

describe('Bicicleta.allBicis', ()=> {
    it('starts empty', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})

describe('Bicicleta.add', ()=> {
    it('Add new bicicleta', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-1, -1]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    })
})

describe('Bicicleta.findById', () => {
    it('bicicleta with ID 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-1, -1]);
        var b = new Bicicleta(2, 'azul', 'montaña', [-2, -2]);
        
        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(1);
        
        expect(targetBici.id_).toBe(1);
        expect(targetBici.color_).toBe(a.color_);
        expect(targetBici.modelo_).toBe(a.modelo_);
    })
})

describe('Bicicleta.removeById', () => {
    it('bicicleta with ID 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-1, -1]);
        var b = new Bicicleta(2, 'azul', 'montaña', [-2, -2]);
        
        Bicicleta.add(a);
        Bicicleta.add(b);

        Bicicleta.removeById(1);

        expect(Bicicleta.allBicis.length).toBe(1);
    })
})
*/