const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');


let mailConfig;

if (process.env.NODE_ENV === 'production') {
    //const options = { auth: { api_key: process.env.SENDGRID_API_KEY } };

    //mailConfig = sgTransport(options);
    const sgMail = require('@sendgrid/mail');
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
        to: 'test@example.com',
        from: 'test@example.com',
        subject: 'Sending with Twilio SendGrid is Fun',
        text: 'and easy to do anywhere, even with Node.js',
        html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    };
    sgMail.send(msg);
} else {
    if (process.env.NODE_ENV === 'staging') {
        console.log("xxxxxxxxxxxxxx");

        const options = { auth: { api_key: process.env.SENDGRID_API_KEY } };

        mailConfig = sgTransport(options);
    }
    else {
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd 
            }
        });
    }
}

module.exports = nodemailer.createTransport(mailConfig);