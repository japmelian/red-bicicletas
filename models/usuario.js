var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
const uniqueValidator = require('mongoose-unique-validator');
const saltRound = 10;
const crypto = require('crypto');
const nodemailer = require('nodemailer');

const Token = require('../models/token');

const validateEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
}

var usuarioSchema = new Schema({
    nombre : {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email : {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingresar un email válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpire: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function(next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRound);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.method.allUsers = function( req, res ){
    return this.find({},cb);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, callback) {
    var reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta });
    console.log(reserva);
    reserva.save(callback);
};

usuarioSchema.statics.add = function(aUser, callback){
    console.log(aUser);
    this.create(aUser, callback);
};

usuarioSchema.statics.updateUser = function(userObj, callback) {
    console.log(userObj.nombre);
    this.updateOne({ _id: userObj._id }, { $set: { nombre: userObj.nombre } }, callback);
};

usuarioSchema.methods.enviar_email_bienvenida = function(callback) {
    console.log("enviando email de bienvenida");

    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });

    console.log(token);
    const email_destination = this.email;
    console.log(email_destination);

    token.save(function(err) {
        if (err) { return console.log(err.message); }

        // Create a SMTP transporter object
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'alexandra.mcglynn13@ethereal.email',
                pass: '668N8EvUt8Aa7g6sDY'
            }
        });

        // Message object
        let message = {
            from: 'Sender Name <sender@example.com>',
            to: 'Recipient <recipient@example.com>',
            subject: 'Nodemailer is unicode friendly ✔',
            text: 'http://localhost:3000' + '\/token/confirmation\/' + token.token,
            html: '<p><b>Hello</b> to myself!</p>'
        };

        transporter.sendMail(message, (err, info) => {
            if (err) { return console.log(err.message); }
    
            console.log('A verification email has been sent to' + email_destination)
            console.log('Message sent: %s', info.messageId);
        });
    });
};

usuarioSchema.methods.resetPassword = function(callback) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;

    token.save(function(err) {
        if (err) { return callback(err); }

        // Create a SMTP transporter object
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'alexandra.mcglynn13@ethereal.email',
                pass: '668N8EvUt8Aa7g6sDY'
            }
        });

        // Message object
        let message = {
            from: 'Sender Name <sender@example.com>',
            to: 'Recipient <recipient@example.com>',
            subject: 'resetear',
            text: 'Resetear http://localhost:3000' + '\/resetPassword\/' + token.token,
            html: '<p><b>Hello</b> to myself!</p>'
        };

        transporter.sendMail(message, (err, info) => {
            if (err) { return console.log(err.message); }
    
            console.log('A verification email has been sent to' + email_destination)
            console.log('Message sent: %s', info.messageId);
        });

        callback(null);
    })
}


module.exports = mongoose.model('Usuario', usuarioSchema);