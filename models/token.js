var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var tokenSchema = new Schema({
    _userId: { type : mongoose.Schema.Types.ObjectId, required: true, ref: 'Usuario' },
    token: { type: String, required: true },
    createAt: { type: Date, required: true, default: Date, expires: 43200 },
});

module.exports = mongoose.model('Token', tokenSchema);