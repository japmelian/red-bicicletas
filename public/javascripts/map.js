var mymap = L.map('mapid').setView([28.466652, -16.2458104], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiamFwbWVsaWFuIiwiYSI6ImNrZGJ5OW5sbTBmeDcycm5idTc5a3BzenMifQ.vl7-OR1SCM7FOGfou2-iPw'
}).addTo(mymap);

$.ajax({
	dataType: "json",
	url: "api/bicicletas",
	success: function(result) {
		console.log(result);
		result.bicicletas.forEach(function(bici) {
			L.marker(bici.ubicacion_, { title: bici.id_ }).addTo(mymap);
		})
	}
})